function componentWrapper() {

    const setTimer = async () => {
        var timeleft = await getTimer();
        if (timeleft <= 0) {
            timeleft = 30;
        }
        var timer = setInterval(async () => {
            document.getElementById("countdown").textContent = timeleft+"s";
            timeleft--;
            saveTimer(timeleft);
            if (timeleft < 0 || timeleft == undefined) {
                clearInterval(timer);
                handleNewWinners();
                startInterval();
            }
        },1000);
    }

    const startInterval = () => {

        setTimer();
        getLotteryNumber();
        getParticipants();
        if (document.getElementById('previous-winners').childElementCount < 1) {
            showPreviousWinners();
        }
    }

    const button = document.getElementById('submit-btn').addEventListener('click', () => {
        addParticipant();
    })

    const handleNewWinners = async() => {
        const participants = await getParticipants();
        const winningNumber = await getLotteryNumber();
        let winners = [];
        participants.forEach(participant => {
            if (participant.number == winningNumber) {
                winners.push(participant.name)
            }
        });
        await addWinners(winners, winningNumber);
        showPreviousWinners();
        removeParticipants();
    }

    const showPreviousWinners = async() => {
       const winners = await getPreviousWinners();
       console.log(winners)
       document.getElementById('previous-winners').innerHTML = '';
       winners.reverse().forEach(data => {
            const element = document.createElement('div');
            element.classList.add('winner');

            const elementNames = document.createElement('div');
            elementNames.classList.add('names')

            const elementNumber = document.createElement('div');
            elementNumber.classList.add('number')

            if (data.names.length < 1) {
                elementNames.innerHTML = 'No lucky contestants';
            } else {
                elementNames.innerHTML = data.names.join(', ');
            }

            elementNumber.innerHTML = '#' + data.number;
            element.appendChild(elementNames);
            element.appendChild(elementNumber);
            document.getElementById('previous-winners').appendChild(element)
        });
    }





    // GET / SET DATABASE FUNCTIONS
    const getParticipants = async () => {
        const uri = 'http://localhost:3000/participants';
        const rest = await fetch(uri);
        const data = await rest.json();
        return data;
    }

    const addWinners = async (winners, winningNumber) => {
        return fetch('http://localhost:3000/winners', {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                names: winners,
                number: winningNumber
            })
        });
    }

    const getPreviousWinners = async () => {
        const uri = 'http://localhost:3000/winners';
        const rest = await fetch(uri);
        const data = await rest.json();
        return data;
    }

    const addParticipant = async () => {
        const name = document.getElementById('name').value;
        const number = document.getElementById('number').value;
        return fetch('http://localhost:3000/participants', {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                name: name,
                number: number
            })
        });
    }

    const removeParticipants = async () => {
        const participants = await getParticipants();
        participants.forEach(element => {
            fetch("http://localhost:3000/participants/" + element.id, {
                method: "DELETE",
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({})
            });
        });

    }

    const getLotteryNumber = async () => {
        const uri = 'http://localhost:8010/proxy/api/getLotteryNumber';
        const rest = await fetch(uri);
        const data = await rest.json();
        // return 2
        return data.lotteryNumber;
    }

    const saveTimer = async (timer) => {
        return fetch('http://localhost:3000/timer/1', {
            method: "PUT",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                id: 1,
                timer: timer
            })
        });
    }

    const getTimer = async () => {
        const uri = 'http://localhost:3000/timer/1';
        const rest = await fetch(uri);
        const data = await rest.json();
        return data.timer;
    }
    startInterval();
}
window.addEventListener('DOMContentLoaded', () => componentWrapper());