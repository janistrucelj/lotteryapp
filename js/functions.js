function wrapper() {
    console.log('hello there!')
    const components = [];
    document.getElementById('add-instance').addEventListener("click", function() {
       console.log('add') 
       const container = document.getElementById('component-container');
       const component = document.createElement('object');
       component.data = 'component.html';
       component.setAttribute('api-rul','http://localhost:8010/proxy/api/getLotteryNumber')
       container.appendChild(component);
       components.push(component);
    }); 
}
window.addEventListener('DOMContentLoaded', () => wrapper());